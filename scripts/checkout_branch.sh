#!/bin/sh

if [ -z "$1" ]
then
  echo "no branch name"
  return 0
fi

OUTPUT=""

echo "gclient sync -RD --with_branch_heads --with_tags -j400"
OUTPUT=`gclient sync -RD --with_branch_heads --with_tags -j400`

echo "git fetch"
OUTPUT=`git fetch`

echo "git checkout -b $1 $1"
OUTPUT=`git checkout -b $1 $1`

echo "gclient sync -RD --with_branch_heads --with_tags -j400"
OUTPUT=`gclient sync -RD --with_branch_heads --with_tags -j400`

