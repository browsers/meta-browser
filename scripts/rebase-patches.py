#! /bin/env python3

import argparse
import os
import pickle
import re
import subprocess
import sys


'''
Automates patch rebasing for all patches that don't have conflicts.

We don't use a regular Chromium checkout, because that doesn't work well when
making changes to nested git repos (like v8 or third_party/ modules). Instead,
we download the tarball for the new version, extract it, initialize an empty git
repo, remove any .gitignore files (because else e.g. v8/ would ge ignored) and
add all the files that are touched by the existing patches (because adding all
files takes way too long).
Then we can just apply the patches one by one, generate the updated diff, and
update the patch file.

Patches with conflicts need to be rebased manually. The script can then be run
with the `-c` / `--continue` flag to rebase the remaining patches. We keep track
of which patches have already been rebased in a pickled list in WORKDIR.

DEPENDENCIES (debian):
    - fd-find
    - git
    - patchutils
    - python3-requests
    - tar
    - wget
'''


YOCTO_PATCH_DIR = '/home/max/yocto-chromium/sources/meta-browser/meta-chromium/recipes-browser/chromium/files'
YOCTO_PATCH_SUBDIRS = ['arm']
YOCTO_BACKPORT_SUBDIR = 'backport'
# Contains the downloaded tarball and the repo based on its extracted contents.
WORKDIR = '/home/max/patch-rebasing'
CHROMIUM_CHECKOUT = os.path.join(WORKDIR, 'chromium')
PICKLE_FILE = os.path.join(WORKDIR, '.rebased-patches.pickle')


def exit(msg):
    print(msg)
    sys.exit(0)


def abort(msg):
    print(msg)
    sys.exit(1)


def log_verbose(msg, verbose):
    if verbose:
        print(msg)


def cmd(args, cwd):
    try:
        return subprocess.run(args.split(' '), capture_output=True, check=True, cwd=cwd)
    except subprocess.CalledProcessError as e:
        print(f'Command failed. stderr:\n{e.stderr}\nstdout:\n{e.stdout}')
        raise e


def staged_changes(cwd):
    res = cmd('git diff --staged --name-only --relative', cwd)
    stdout = res.stdout.decode().strip()
    if stdout == '':
        return []
    return stdout.split('\n')


def rebased_patches():
    if os.path.exists(PICKLE_FILE):
        with open(PICKLE_FILE, 'rb') as f:
            return pickle.load(f)
    return set()


def patches_to_rebase(include_backports=True):
    all_patches = set(f for f in os.listdir(
        YOCTO_PATCH_DIR) if f.endswith('.patch'))

    subdirs = list(YOCTO_PATCH_SUBDIRS)
    if include_backports:
        if os.path.exists(os.path.join(YOCTO_PATCH_DIR, YOCTO_BACKPORT_SUBDIR)):
            subdirs.append(YOCTO_BACKPORT_SUBDIR)
    for subdir in subdirs:
        all_patches.update(set(os.path.join(subdir, f) for f in os.listdir(os.path.join(YOCTO_PATCH_DIR, subdir)) if f.endswith('.patch')))

    rebased = rebased_patches()
    return sorted(list(all_patches.difference(rebased)))


def determine_backports_to_rebase():
    subdir = YOCTO_BACKPORT_SUBDIR
    try:
        backports = set(os.path.join(subdir, f) for f in os.listdir(os.path.join(YOCTO_PATCH_DIR, subdir)) if f.endswith('.patch'))
    except FileNotFoundError:
        # No backport subdir, thus no backports.
        print('WARNING: backport subdirectory doesn\'t exist, assuming there are no backported patches.')
        return list()

    rebased = rebased_patches()
    unrebased_backports = backports.difference(rebased)
    to_rebase = set()
    dropped = set()

    for backport in unrebased_backports:
        with open(os.path.join(YOCTO_PATCH_DIR, backport), 'r') as b:
            contents = b.read()
            # Omit the actual diff.
            message = contents[:contents.find('---')]
            print(f"Found backport '{backport}':")
            print(message)

        choice = ''
        while choice not in ['rebase', 'drop']:
            choice = input('What should we do with the backport? [rebase/drop] ').lower()
        if choice == 'rebase':
            to_rebase.add(backport)
        else:
            os.remove(os.path.join(YOCTO_PATCH_DIR, backport))
            dropped.add(backport)

    print('Dropped the following backports:')
    for backport in dropped:
        print(f'- {backport}')
    print("Don't forget to remove them from SRC_URI.")

    return sorted(list(to_rebase))


def download_and_extract_tarball(version):
    tarball = f'chromium-{version}.tar.xz'
    url = 'https://commondatastorage.googleapis.com/chromium-browser-official/' + tarball
    print('        Downloading tarball...')
    cmd('wget -c ' + url, WORKDIR)
    print('        Extracting tarball...')
    cmd('tar -xf ' + tarball, WORKDIR)
    cmd(f'mv chromium-{version} {CHROMIUM_CHECKOUT}', WORKDIR)


def reformat_diff(diff):
    # Change lines that only contain whitespace to be empty
    diff = '\n'.join([line if not line.isspace()
                     else '' for line in diff.splitlines()])
    # Add final newline
    return diff + '\n'


def apply_patch(patch, verbose):
    with open(os.path.join(YOCTO_PATCH_DIR, patch), 'r') as p:
        patch_cmd = ['patch', '-p1', '-r', '-', '--no-backup-if-mismatch']
        try:
            subprocess.run(patch_cmd, cwd=CHROMIUM_CHECKOUT,
                           stdin=p, stdout=subprocess.PIPE, check=True)
        except subprocess.CalledProcessError as e:
            cmd('git reset --hard HEAD', CHROMIUM_CHECKOUT)
            log_verbose(f'Patch conflict:\n{e.output.decode()}', verbose)
            exit('Patch conflict. Please rebase this patch manually and then run\n' +
                 f'`{sys.argv[0]} --fixed-manually {patch}`\nand `{sys.argv[0]} --continue`.')
    cmd('git add -u', CHROMIUM_CHECKOUT)
    cmd('git commit -m ' + patch, CHROMIUM_CHECKOUT)
    diff = cmd('git diff HEAD^', CHROMIUM_CHECKOUT).stdout.decode()
    return reformat_diff(diff)


def update_patch_file(patch, diff):
    with open(os.path.join(YOCTO_PATCH_DIR, patch), 'r+') as p:
        # Seek to where the actual diff starts in the patch file, so that we don't touch
        # the patch message etc.
        p.seek(p.read().find('diff --git'))
        p.write(diff)
        p.truncate()


def update_pickle_file(rebased_patch):
    new_rebased_patches = rebased_patches()
    new_rebased_patches.add(rebased_patch)
    with open(PICKLE_FILE, 'wb') as f:
        pickle.dump(new_rebased_patches, f, pickle.HIGHEST_PROTOCOL)


def rebase_patch(patch, verbose):
    print(f'Rebasing {patch}...')

    log_verbose('    Applying patch...', verbose)
    diff = apply_patch(patch, verbose)

    log_verbose('    Updating patch file with new diff...', verbose)
    update_patch_file(patch, diff)

    log_verbose('    Saving state...', verbose)
    update_pickle_file(patch)

    log_verbose('    Done.', verbose)


def main():
    parser = argparse.ArgumentParser(prog=sys.argv[0],
                                     description='Rebase meta-browser patches')
    parser.add_argument('-f', '--fixed-manually')
    parser.add_argument('-c', '--continue',
                        action='store_true', dest='continue_')
    parser.add_argument('-v', '--verbose', action='store_true')
    parser.add_argument('version', nargs='?', default=None)
    args = parser.parse_args()

    if (fixed_patch := args.fixed_manually) is not None:
        update_pickle_file(fixed_patch)
        exit(f'Marked {fixed_patch} as manually fixed.')

    if not args.continue_:
        if args.version is None:
            abort('version argument is required.')

        if re.fullmatch(r'\d{3}\.0\.\d{4}\.\d{1,3}', args.version) is None:
            abort(f'"{version}" doesn\'t look like a Chromium version number.')

        print('Preparing...')

        if not os.path.exists(WORKDIR):
            res = cmd(f'mkdir -p {WORKDIR}', os.getcwd())

        if os.path.exists(PICKLE_FILE):
            os.remove(PICKLE_FILE)
        
        if os.path.exists(CHROMIUM_CHECKOUT):
            abort(f'{CHROMIUM_CHECKOUT} exists already. ' +
                   'Please delete it before starting the rebase.')

        print('   Downloading and extracting tarball...')
        download_and_extract_tarball(args.version)

        print('   Initializing git repo for extracted tarball...')
        # Remove .gitignore files
        cmd('fdfind -I -H .gitignore -x rm', CHROMIUM_CHECKOUT)

        all_affected_files = set()
        all_patches = patches_to_rebase(include_backports=False)
        all_patches += determine_backports_to_rebase()

        for patch in all_patches:
            affected = cmd(
                f'lsdiff --strip=1 {patch}', YOCTO_PATCH_DIR).stdout.decode().splitlines()
            all_affected_files = all_affected_files.union(affected)

        # If a patch touches a file that is (re)moved upstream, trying to `git add` it will fail
        # because it doesn't exist. Such a patch will need to be rebased manually anyway, so we can
        # just ignore affected files that don't exist any more.
        # Iterate over a copy of the set so that we can modify it while iterating.
        for file in all_affected_files.copy():
            if not os.path.exists(os.path.join(CHROMIUM_CHECKOUT, file)):
                print(f"Ignoring '{file}', which was modified by a patch but (re)moved upstream.")
                all_affected_files.remove(file)

        cmd('git init', CHROMIUM_CHECKOUT)
        cmd('git add ' + ' '.join(all_affected_files), CHROMIUM_CHECKOUT)
        cmd('git commit -m initial', CHROMIUM_CHECKOUT)

        print('   Done.')

    for patch in patches_to_rebase():
        rebase_patch(patch, args.verbose)

    print('Finished rebasing all patches!')


if __name__ == '__main__':
    main()
