#!/bin/sh

echo "Renaming chromium-ozone-wayland to chromium-ozone-wayland_$1.bb"
git mv chromium-ozone-wayland_* chromium-ozone-wayland_$1.bb
echo "Renaming chromium-x11 to chromium-x11_$1.bb"
git mv chromium-x11_* chromium-x11_$1.bb
echo "Renaming gn-native to gn-native_$1.bb"
git mv gn-native_* gn-native_$1.bb
echo "Done!\n"
