#!/bin/sh

if [ ! -e "chromium-$1.tar.xz" ]; then
    echo "File chromium-$1.tar.xz doesn't exists. Downloading..."
    wget https://commondatastorage.googleapis.com/chromium-browser-official/chromium-$1.tar.xz
else
    echo "File chromium-$1.tar.xz exists."
fi

echo "Checking SHA256SUM now."

SHA256SUM=`sha256sum chromium-$1.tar.xz | awk '{ print $1 }'`

git grep -l "SRC_URI\[sha256sum\]" | xargs sed -i "s/^SRC_URI\[sha256sum\].*/SRC_URI\[sha256sum\] = \"$SHA256SUM\"/gi"

echo "Done!"
